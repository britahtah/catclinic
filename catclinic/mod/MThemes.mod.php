<?php

class MThemes
{
    // Connexion à la Base de Données
    private $conn;

    // Clef primaire de la table THEMES
    private $id_theme;

    // Tableau de gestion de données (insert ou update)
    private $value;

    // Constructeur de la class MDocuments
    // Instancie le membre privé $conn
    public function __construct($_id_theme = null)
    {
        // Connexion à la Base de Données
        $this->conn = new PDO(DATABASE, LOGIN, PASSWORD);

        // Instanciation du membre $id_doc
        $this->id_theme = $_id_theme;

        return;

    } // __construct()

    public function __destruct()
    {
    }

    public function SetValue($_value)
    {
        $this->value = $_value;
        return;
    } // SetValue($_value)

    public function SelectAll()
    {
        $query =    'select ID_THEME, NOM_THEME
                    from themes';

        $result = $this->conn->prepare($query);
        $result->execute();

        return $result->fetchAll();
    } // SelectAll()

    public function SelectTheme()
    {
        $query = 'select * from themes
                  where ID_THEME = :ID_THEME';

        $result = $this->conn->prepare($query);
        $result->bindValue(':ID_THEME', $this->id_theme, PDO::PARAM_INT);

        $result->execute();
        return $result->fetch();

    } // Select()

    public function InsertTheme()
    {

        $query =    'insert into themes (NOM_THEME)
                    values(:NOM_THEME)';

        $result = $this->conn->prepare($query);


        $result->bindValue(':NOM_THEME', $this->value['NOM_THEME'], PDO::PARAM_STR);

        $result->execute();

        return;

    } // InsertTheme()

    public function UpdateTheme()
    {
        $query = 'update themes 
                  set NOM_THEME = :NOM_THEME   
                  where ID_THEME = :ID_THEME';

        $result = $this->conn->prepare($query);

        $result->bindValue(':NOM_THEME', $this->value['NOM_THEME'], PDO::PARAM_STR);
        $result->bindValue(':ID_THEME', $this->id_theme, PDO::PARAM_INT);

        $result->execute();

        return;

    } // Update()


    public function DeleteTheme()
    {
        /*
        $nbrFiche = $this->CountFiche();
 var_dump($nbrFiche);

         if ( $nbrFiche[1] = 0){
             $query = 'DELETE FROM themes WHERE ID_THEME = :ID_THEME';

             $result = $this->conn->prepare($query);
             $result->bindValue(':ID_THEME', $this->id_theme, PDO::PARAM_INT);
             $result->execute();
         }
         else {
             echo '<script>alert("BOUDIOU");</script>';
         }
*/
        $query =    'DELETE FROM themes 
                    WHERE ID_THEME = :ID_THEME';

        $result = $this->conn->prepare($query);
        $result->bindValue(':ID_THEME', $this->id_theme, PDO::PARAM_INT);
        $result->execute();

        return;

    } // DeleteTheme()
    public function CountFiche()
    {
        $query =      'select count(ID_FICHE) 
                      from fiches F, themes T
                      where  F.ID_THEME = T.ID_THEME
                      and T.ID_THEME = :ID_THEME';

        $result = $this->conn->prepare($query);
        $result->bindValue(':ID_THEME', $this->id_theme, PDO::PARAM_INT);
        $result->execute();

        return $result->fetch();
    }

}
?>
