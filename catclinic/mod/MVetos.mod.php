<?php

class MVetos
{
    private $conn;
    private $id_veto;
    private $value;

    public function __construct($_id_veto = null)
    {
        // Connexion à la Base de Données
        $this->conn = new PDO(DATABASE, LOGIN, PASSWORD);

        // Instanciation du membre $id_doc
        $this->id_veto = $_id_veto;

        return;

    } // __construct()

    public function __destruct()
    {
    }

    public function SetValue($_value)
    {
        $this->value = $_value;
        return;
    } // SetValue($_value)

    public function SelectAll()
    {
        $query =    'select ID_VETO, NOM_VETO, PROFESSION_VETO, IMAGE
                    from veterinaires';

        $result = $this->conn->prepare($query);
        $result->execute();

        return $result->fetchAll();
    } // SelectAll()

    public function SelectVeto()
    {
        $query = 'select * from veterinaires
                  where ID_VETO = :ID_VETO';

        $result = $this->conn->prepare($query);
        $result->bindValue(':ID_VETO', $this->id_veto, PDO::PARAM_INT);

        $result->execute();
        return $result->fetch();
    } // SelectVeto()

    public function InsertVeto()
    {
        $query =    'insert into veterinaires (NOM_VETO, PROFESSION_VETO, IMAGE, DESCRIPTION_VETO, MAIL_VETO, PASSWORD)
                    values(:NOM_VETO, :PROFESSION_VETO, :IMAGE, :DESCRIPTION_VETO, :MAIL_VETO, :PASSWORD)';

        $result = $this->conn->prepare($query);


        $result->bindValue(':NOM_VETO', $this->value['NOM_VETO'], PDO::PARAM_STR);
        $result->bindValue(':IMAGE', $this->value['IMAGE'], PDO::PARAM_STR);
        $result->bindValue(':PROFESSION_VETO', $this->value['PROFESSION_VETO'], PDO::PARAM_STR);
        $result->bindValue(':DESCRIPTION_VETO', $this->value['DESCRIPTION_VETO'], PDO::PARAM_STR);
        $result->bindValue(':MAIL_VETO', $this->value['MAIL_VETO'], PDO::PARAM_STR);
        $result->bindValue(':PASSWORD', $this->value['PASSWORD'], PDO::PARAM_STR);

        $result->execute();
        return;
    } // InsertVeto()

    public function UpdateVeto()
    {
        if(isset($this->value['IMAGE']))
        {
            $query =    'update veterinaires 
                        set NOM_VETO = :NOM_VETO, IMAGE = :IMAGE, PROFESSION_VETO = :PROFESSION_VETO, DESCRIPTION_VETO = :DESCRIPTION_VETO, MAIL_VETO = :MAIL_VETO, PASSWORD = :PASSWORD      
                        where ID_VETO = :ID_VETO';

            $result = $this->conn->prepare($query);

            $result->bindValue(':NOM_VETO', $this->value['NOM_VETO'], PDO::PARAM_STR);
            $result->bindValue(':IMAGE', $this->value['IMAGE'], PDO::PARAM_STR);
            $result->bindValue(':PROFESSION_VETO', $this->value['PROFESSION_VETO'], PDO::PARAM_STR);
            $result->bindValue(':DESCRIPTION_VETO', $this->value['DESCRIPTION_VETO'], PDO::PARAM_STR);
            $result->bindValue(':MAIL_VETO', $this->value['MAIL_VETO'], PDO::PARAM_STR);
            $result->bindValue(':PASSWORD', $this->value['PASSWORD'], PDO::PARAM_STR);
            $result->bindValue(':ID_VETO', $this->id_veto, PDO::PARAM_INT);

            $result->execute();

        }
        else
            {
            $query =    'update veterinaires 
                        set NOM_VETO = :NOM_VETO, PROFESSION_VETO = :PROFESSION_VETO, DESCRIPTION_VETO = :DESCRIPTION_VETO, MAIL_VETO = :MAIL_VETO, PASSWORD = :PASSWORD      
                        where ID_VETO = :ID_VETO';

            $result = $this->conn->prepare($query);

            $result->bindValue(':NOM_VETO', $this->value['NOM_VETO'], PDO::PARAM_STR);
            $result->bindValue(':PROFESSION_VETO', $this->value['PROFESSION_VETO'], PDO::PARAM_STR);
            $result->bindValue(':DESCRIPTION_VETO', $this->value['DESCRIPTION_VETO'], PDO::PARAM_STR);
            $result->bindValue(':MAIL_VETO', $this->value['MAIL_VETO'], PDO::PARAM_STR);
            $result->bindValue(':PASSWORD', $this->value['PASSWORD'], PDO::PARAM_STR);
            $result->bindValue(':ID_VETO', $this->id_veto, PDO::PARAM_INT);

            $result->execute();
        }

        return;

    } // UpdateVeto()

    public function DeleteVeto()
    {
        $query =    'DELETE FROM veterinaires 
                    WHERE ID_VETO = :ID_VETO';

        $result = $this->conn->prepare($query);
        $result->bindValue(':ID_VETO', $this->id_veto, PDO::PARAM_INT);
        $result->execute();

        return;

    } // DeleteVeto()
}
