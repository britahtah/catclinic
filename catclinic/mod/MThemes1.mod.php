<?php

class MThemes
{
    private $conn;
    private $id_theme;
    private $value;

    public function __construct($_id_theme = null)
    {
        // Connexion à la Base de Données
        $this->conn = new PDO(DATABASE, LOGIN, PASSWORD);

        // Instanciation du membre $id_doc
        $this->id_theme = $_id_theme;

        return;

    } // __construct()

    public function __destruct()
    {
    }

    public function SetValue($_value)
    {
        $this->value = $_value;
        return;
    } // SetValue($_value)

    public function SelectAll()
    {
        $query = 'select ID_THEME, NOM_THEME
              from themes';

        $result = $this->conn->prepare($query);
        $result->execute();

        return $result->fetchAll();
    } // SelectAll()
}
