<?php

class MFiches
{
    private $conn;
    private $id_fiche;
    private $value;

    public function __construct($_id_fiche = null)
    {
        // Connexion à la Base de Données
        $this->conn = new PDO(DATABASE, LOGIN, PASSWORD);

        // Instanciation du membre $id_fiche
        $this->id_fiche = $_id_fiche;

        return;

    } // __construct()
    public function __destruct() {}

    public function SetValue($_value)
    {
        $this->value = $_value;
        return;
    } // SetValue($_value)

    public function SelectAll()
    {
        $query =    'select ID_FICHE, NOM_FICHE, NOM_THEME, IMAGE
                    from fiches F, themes T
                    where F.ID_THEME = T.ID_THEME';

        $result = $this->conn->prepare($query);
        $result->execute();

        return $result->fetchAll();
    } // SelectAll()

    public function Select()
    {
        $query =    'select * from fiches
                    where ID_FICHE = :ID_FICHE';

        $result = $this->conn->prepare($query);
        $result->bindValue(':ID_FICHE', $this->id_fiche, PDO::PARAM_INT);

        $result->execute();
        return $result->fetch();

    } // Select()

    public function Insert()
    {
        $query =    "insert into fiches (NOM_FICHE, CONTENU_FICHE, ID_THEME, IMAGE)
                    values(:NOM_FICHE, :CONTENU_FICHE, :ID_THEME, :IMAGE)";

        $result = $this->conn->prepare($query);

        $result->bindValue(':NOM_FICHE', $this->value['NOM_FICHE'], PDO::PARAM_STR);
        $result->bindValue(':CONTENU_FICHE', $this->value['CONTENU_FICHE'], PDO::PARAM_STR);
        $result->bindValue(':ID_THEME', $this->value['ID_THEME'], PDO::PARAM_INT);
        $result->bindValue(':IMAGE', $this->value['IMAGE'], PDO::PARAM_STR);

        $result->execute();

        return;

    } // Insert()

    public function Update()
    {
        if(isset($this->value['IMAGE']))
        {
            $query =  'update fiches 
                      set NOM_FICHE = :NOM_FICHE, 
                      CONTENU_FICHE = :CONTENU_FICHE, 
                      ID_THEME = :ID_THEME,
                      IMAGE = :IMAGE 
                      where ID_FICHE = :ID_FICHE';

            $result = $this->conn->prepare($query);

            $result->bindValue(':NOM_FICHE', $this->value['NOM_FICHE'], PDO::PARAM_STR);
            $result->bindValue(':CONTENU_FICHE', $this->value['CONTENU_FICHE'], PDO::PARAM_STR);
            $result->bindValue(':ID_FICHE', $this->id_fiche, PDO::PARAM_INT);
            $result->bindValue(':ID_THEME', $this->value['ID_THEME'], PDO::PARAM_INT);
            $result->bindValue(':IMAGE', $this->value['IMAGE'], PDO::PARAM_STR);


            $result->execute();
        }
        else
            {
            $query =      'update fiches 
                          set NOM_FICHE = :NOM_FICHE, 
                          CONTENU_FICHE = :CONTENU_FICHE, 
                          ID_THEME = :ID_THEME
                          where ID_FICHE = :ID_FICHE';

            $result = $this->conn->prepare($query);

            $result->bindValue(':NOM_FICHE', $this->value['NOM_FICHE'], PDO::PARAM_STR);
            $result->bindValue(':CONTENU_FICHE', $this->value['CONTENU_FICHE'], PDO::PARAM_STR);
            $result->bindValue(':ID_FICHE', $this->id_fiche, PDO::PARAM_INT);
            $result->bindValue(':ID_THEME', $this->value['ID_THEME'], PDO::PARAM_INT);

            $result->execute();
        }
        return;

    } // Update()

    public function Delete()
    {
        $query =    'DELETE FROM fiches 
                    WHERE ID_FICHE = :ID_FICHE';

        $result = $this->conn->prepare($query);
        $result->bindValue(':ID_FICHE', $this->id_fiche, PDO::PARAM_INT);
        $result->execute();

        return;

    } // Delete()

}