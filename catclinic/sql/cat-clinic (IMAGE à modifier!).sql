#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: FICHES
#------------------------------------------------------------

CREATE TABLE FICHES(
        ID_FICHE      int (11) Auto_increment  NOT NULL ,
        NOM_FICHE     Varchar (50) NOT NULL ,
        CONTENU_FICHE Text NOT NULL ,
        PHOTO_FICHE   Text NOT NULL ,
        ID_THEME      Int NOT NULL ,
        PRIMARY KEY (ID_FICHE )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: THEMES
#------------------------------------------------------------

CREATE TABLE THEMES(
        ID_THEME  int (11) Auto_increment  NOT NULL ,
        NOM_THEME Varchar (50) NOT NULL ,
        PRIMARY KEY (ID_THEME )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: MOTS_CLES
#------------------------------------------------------------

CREATE TABLE MOTS_CLES(
        ID_MOTCLE  int (11) Auto_increment  NOT NULL ,
        NOM_MOTCLE Varchar (50) NOT NULL ,
        PRIMARY KEY (ID_MOTCLE )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: VETERINAIRES
#------------------------------------------------------------

CREATE TABLE VETERINAIRES(
        ID_VETO          int (11) Auto_increment  NOT NULL ,
        NOM_VETO         Varchar (50) NOT NULL ,
        PHOTO_VETO       Text NOT NULL ,
        PASSWORD         Varchar (25) ,
        PROFESSION_VETO  Varchar (50) NOT NULL ,
        DESCRIPTION_VETO Text NOT NULL ,
        MAIL_VETO        Varchar (50) NOT NULL ,
        PRIMARY KEY (ID_VETO )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: FICHES_MOTSCLES
#------------------------------------------------------------

CREATE TABLE FICHES_MOTSCLES(
        ID_FICHE  Int NOT NULL ,
        ID_MOTCLE Int NOT NULL ,
        PRIMARY KEY (ID_FICHE ,ID_MOTCLE )
)ENGINE=InnoDB;

ALTER TABLE FICHES ADD CONSTRAINT FK_FICHES_ID_THEME FOREIGN KEY (ID_THEME) REFERENCES THEMES(ID_THEME);
ALTER TABLE FICHES_MOTSCLES ADD CONSTRAINT FK_FICHES_MOTSCLES_ID_FICHE FOREIGN KEY (ID_FICHE) REFERENCES FICHES(ID_FICHE);
ALTER TABLE FICHES_MOTSCLES ADD CONSTRAINT FK_FICHES_MOTSCLES_ID_MOTCLE FOREIGN KEY (ID_MOTCLE) REFERENCES MOTS_CLES(ID_MOTCLE);
