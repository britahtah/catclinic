-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Mer 17 Janvier 2018 à 15:51
-- Version du serveur :  10.1.21-MariaDB
-- Version de PHP :  7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `catclinic`
--

-- --------------------------------------------------------

--
-- Structure de la table `fiches`
--

CREATE TABLE `fiches` (
  `ID_FICHE` int(11) NOT NULL,
  `NOM_FICHE` varchar(50) NOT NULL,
  `CONTENU_FICHE` text NOT NULL,
  `ID_THEME` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `fiches`
--

INSERT INTO `fiches` (`ID_FICHE`, `NOM_FICHE`, `CONTENU_FICHE`, `ID_THEME`) VALUES
(3, 'tes', 'j\'y crois encore!', 1),
(4, 'pok', 'bulbi', 1);

-- --------------------------------------------------------

--
-- Structure de la table `fiches_motscles`
--

CREATE TABLE `fiches_motscles` (
  `ID_FICHE` int(11) NOT NULL,
  `ID_MOTCLE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `mots_cles`
--

CREATE TABLE `mots_cles` (
  `ID_MOTCLE` int(11) NOT NULL,
  `NOM_MOTCLE` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `themes`
--

CREATE TABLE `themes` (
  `ID_THEME` int(11) NOT NULL,
  `NOM_THEME` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `themes`
--

INSERT INTO `themes` (`ID_THEME`, `NOM_THEME`) VALUES
(1, 'test'),
(2, 'prout');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `fiches`
--
ALTER TABLE `fiches`
  ADD PRIMARY KEY (`ID_FICHE`),
  ADD KEY `FK_FICHES_ID_THEME` (`ID_THEME`);

--
-- Index pour la table `fiches_motscles`
--
ALTER TABLE `fiches_motscles`
  ADD PRIMARY KEY (`ID_FICHE`,`ID_MOTCLE`),
  ADD KEY `FK_FICHES_MOTSCLES_ID_MOTCLE` (`ID_MOTCLE`);

--
-- Index pour la table `mots_cles`
--
ALTER TABLE `mots_cles`
  ADD PRIMARY KEY (`ID_MOTCLE`);

--
-- Index pour la table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`ID_THEME`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `fiches`
--
ALTER TABLE `fiches`
  MODIFY `ID_FICHE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `mots_cles`
--
ALTER TABLE `mots_cles`
  MODIFY `ID_MOTCLE` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `themes`
--
ALTER TABLE `themes`
  MODIFY `ID_THEME` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `fiches`
--
ALTER TABLE `fiches`
  ADD CONSTRAINT `FK_FICHES_ID_THEME` FOREIGN KEY (`ID_THEME`) REFERENCES `themes` (`ID_THEME`);

--
-- Contraintes pour la table `fiches_motscles`
--
ALTER TABLE `fiches_motscles`
  ADD CONSTRAINT `FK_FICHES_MOTSCLES_ID_FICHE` FOREIGN KEY (`ID_FICHE`) REFERENCES `fiches` (`ID_FICHE`),
  ADD CONSTRAINT `FK_FICHES_MOTSCLES_ID_MOTCLE` FOREIGN KEY (`ID_MOTCLE`) REFERENCES `mots_cles` (`ID_MOTCLE`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
