<?php

class VVetos
{
    /**
     * Constructeur
     */
    public function __construct() {}

    /**
     * Destructeur
     */
    public function __destruct() {}

    public function showVetos($_data)
    {
        $tr = '';

        foreach ($_data as $val)
        {
            $img = '';

            if($val['IMAGE'])
            {
                $img = "<img src='../upload/".$val['IMAGE']."'/>";
            }
            $tr .= "<tr><td>" . $val['NOM_VETO'] . "</td><td>" . $val['PROFESSION_VETO'] . "</td><td>".$img."</td><td><a href='../Php/admin.php?EX=formVeto&ID_VETO=" . $val['ID_VETO'] . "'>Modifier</a>&nbsp;&nbsp;&nbsp;<a href='../Php/admin.php?EX=deleteVeto&ID_VETO=" . $val['ID_VETO'] . "'>Supprimer</a></td></tr>";
        }

        $nouveauVeto = '<a href="../Php/admin.php?EX=formVeto"><button class="button">Ajouter un Membre</button></a>';

        echo <<<HERE
<h1>Liste du personnel</h1>
<table>
 <thead>
  <tr>
   <th>Nom</th><th>Profession</th><th>Photo</th><th>Action</th>
  </tr>
 </thead>
 <tbody>
  $tr
 </tbody>
</table>

$nouveauVeto

HERE;
    }

    public function formVeto($_data)
    {
        if ($_data)
        {
            $nom_veto = $_data['NOM_VETO'];
            $profession = $_data['PROFESSION_VETO'];
            $description = $_data['DESCRIPTION_VETO'];
            $photo_veto = $_data['IMAGE'];
            $mail_veto = $_data['MAIL_VETO'];
            $password = $_data['PASSWORD'];
            $action = '../Php/admin.php?EX=updateVeto&ID_VETO=' . $_data['ID_VETO'];
            $button_delete = '<a class="button" href="../Php/admin.php?EX=deleteVeto&amp;ID_VETO=' . $_data['ID_VETO'] . '">Supprimer</a>';
        }
        else
            {
            $nom_veto = '';
            $profession = '';
            $description = '';
            $photo_veto = '';
            $mail_veto = '';
            $password = '';
            $action = '../Php/admin.php?EX=insertVeto';
            $button_delete = '';
            }

            $retour = '<a href="../Php/admin.php?EX=veto"><button class="button">Retour Liste du personnel</button></a>';

        echo <<<HERE
<form action="$action" method="post" enctype="multipart/form-data">
 <fieldset>
  <legend>Formulaire</legend>
  <p>
   <label for="nom">Nom</label>
   <input type="text" id="NOM_VETO" name="NOM_VETO" value="$nom_veto"/>
  </p>
  <p>
   <label for="profession">Profession</label>
   <input type="text" id="PROFESSION_VETO" name="PROFESSION_VETO" value="$profession"/>
  </p>
  <p>
   <label for="description">Description</label>
   <textarea id="DESCRIPTION_VETO" name="DESCRIPTION_VETO"> $description </textarea>
  </p>
  <p>
   <label for="photo">Photo</label>
   <input type="file" id="PHOTO_VETO" name="IMAGE" value=""/><br>$photo_veto
  </p>
  <p>
   <label for="mail">Email</label>
   <input type="email" id="MAIL_VETO" name="MAIL_VETO" value="$mail_veto"/>
  </p>
  <p>
   <label for="password">Mot de passe</label>
   <input type="password" id="PASSWORD" name="PASSWORD" value="$password"/>
  </p>
  <p class="submit">
   <input class="button" type="submit" value="Ok" />
   $button_delete
   $retour
  </p>
 </fieldset>
</form>
HERE;
    }

}
