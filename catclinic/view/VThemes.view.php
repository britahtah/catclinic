<?php

class VThemes
{
    /**
     * Constructeur
     */
    public function __construct() {}

    /**
     * Destructeur
     */
    public function __destruct() {}

    public function showThemes($_data)
    {
        $tr = '';
        foreach ($_data as $val)
        {
            $tr .= "<tr><td>" . $val['NOM_THEME'] . "</td><td><a href='../Php/admin.php?EX=formTheme&ID_THEME=" . $val['ID_THEME'] . "'>Modifier</a>&nbsp;&nbsp;&nbsp;<a href='../Php/admin.php?EX=deleteTheme&ID_THEME=" . $val['ID_THEME'] . "'>Supprimer</a></td></tr>";
        }

        $nouveauTheme = '<a href="../Php/admin.php?EX=formTheme"><button class="button">Ajouter un thème</button></a>';
        $retour = '<a href="../Php/admin.php?EX=liste"><button class="button">Retour Liste des Fiches</button></a>';

        echo <<<HERE
<h1>Liste des thèmes</h1>
<table>
 <thead>
  <tr>
   <th>Nom des thèmes</th><th>Action</th>
  </tr>
 </thead>
 <tbody>
  $tr
 </tbody>
</table>

$nouveauTheme
$retour

HERE;
    }

    public function formThemes($_data)
    {
        if ($_data)
        {
            $nom_theme = $_data['NOM_THEME'];
            $action = '../Php/admin.php?EX=updateTheme&ID_THEME=' . $_data['ID_THEME'];
        }
        else
            {
            $nom_theme = '';
            $action = '../Php/admin.php?EX=insertTheme';
            }

        echo <<<HERE
<form action="$action" method="post">
 <fieldset>
  <legend>Nouveau Thème</legend>
  <p>
   <input name="NOM_THEME" value="$nom_theme"/>
  </p>
  <p class="submit">
   <input class="button" type="submit" value="Ok"/>
  </p>
 </fieldset>
</form>
HERE;

    }
}
