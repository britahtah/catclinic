<?php
// Classe pour l'affichage des fichiers html ou html.php
class VHtml
{

  // Constructeur de la classe VHtml
  public function __construct()
  {}

  // Destructeur de la classe VHtml
  public function __destruct()
  {}

  // showHtml(): Affichage de la page demandée
  public function showHtml($_html)
  {
    // Affichage du fichier $_html s'il existe
    // sinon affichage du fichier unknown.html
    ($_html || file_exists($_html)) ? include($_html) : include('../html/unknown.html');

    return;

  } // showHtml($_html)

} // VHtml
?>
