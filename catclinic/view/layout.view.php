<?php
global $content;
// $vmenu = new VMenu();
$vcontent = new $content['class']();
?>

<!DOCTYPE html>
<html class="no-js" lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php $content['title'] ?></title>
    <link rel="stylesheet" href="../css/app.css">
    <link rel="stylesheet" href="../scss/monstyle.scss"
</head>

<body class="grid-container full">

    <header>

        <div class="title-bar" data-responsive-toggle="nav_menu" data-hide-for="medium">
            <button class="menu-icon" type="button" data-toggle="nav_menu"></button>
            <div class="title-bar-title">Menu</div>
        </div>

        <div class="top-bar" id="nav_menu">
            <div class="top-bar-left">
                CAT CLINIC
            </div>

            <nav class="top-bar-right">
                <ul class="vertical medium-horizontal menu" data-responsive-menu="drilldown medium-dropdown">
                    <li>
                        <a href="index.php?EX=home">Home</a>
                    </li>
                    <li>
                        <a href="index.php?EX=clinique">La clinique</a>
                    </li>
                    <li>
                        <a href="index.php?EX=themes">Fiches conseil</a>
                        <ul class="vertical menu">
                            <li><a href="#">Item 1A</a></li>
                            <li><a href="#">Item 1B</a></li>
                            <li><a href="#">Item 1C</a></li>
                            <li><a href="#">Item 1D</a></li>
                            <li><a href="#">Item 1E</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="index.php?EX=infos">Infos pratiques</a>
                    </li>
                    <li>
                        <a href="index.php?EX=contact">Contact</a>
                    </li>
                </ul>
            </nav>
        </div>

        <div class="cell">
            <img src="../img/kittysleeping.jpg" />
        </div>

    </header>

    <section class="" id="content">
      <?php $vcontent->{$content['method']}($content['arg']) ?>
    </section>

    <footer>
        <a href="#">Copyright</a> | <a href="#">Mentions légales</a> | <a href="#">Plan du site</a>
    </footer>

    <script src="../bower_components/jquery/dist/jquery.js"></script>
    <script src="../bower_components/what-input/dist/what-input.js"></script>
    <script src="../bower_components/foundation-sites/dist/js/foundation.js"></script>
    <script src="../js/app.js"></script>

</body>
</html>
