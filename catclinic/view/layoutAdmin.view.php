<?php
/** Fichier de mise en page */

global $content;
$vcontent = new $content['class']();
?>

<!DOCTYPE html>
<html class="no-js" lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Oswald|Source+Sans+Pro" rel="stylesheet">
    <link rel="stylesheet" type="text/css"  href="../css/app.css">
    <title><?=$content['title']?></title>
</head>

<body>
<div id="content" class="site_main">
    <?php $vcontent->{$content['method']}($content['arg']) ?>
</div>

<script src="../bower_components/jquery/dist/jquery.js"></script>
<script src="../bower_components/what-input/dist/what-input.js"></script>
<script src="../bower_components/foundation-sites/dist/js/foundation.js"></script>
<script src="../js/app.js"></script>

</body>
</html>
