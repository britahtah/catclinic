<?php

class VFiches
{
    /**
     * Constructeur
     */
    public function __construct()
    {
    }

    /**
     * Destructeur
     */
    public function __destruct()
    {
    }

    public function showFiches($_data)
    {
        $tr = '';
        foreach ($_data as $val) {
            $img = '';
            if($val['IMAGE']){
                $img = "<img src='../upload/".$val['IMAGE']."'/>";
            }
            $tr .= "<tr><td>" . $val['NOM_FICHE'] . "</td><td>" . $val['NOM_THEME'] . "</td><td>" .$img. "</td><td><a href='../Php/admin.php?EX=form&ID_FICHE=" . $val['ID_FICHE'] . "'>Modifier</a></td></tr>";
        }

        $nouveau = '<a href="../Php/admin.php?EX=form"><button class="button">Ajouter une fiche</button></a>';
        $nouveauTheme = '<a href="../Php/admin.php?EX=theme"><button class="button">Liste des thèmes</button></a>';

        echo <<<HERE
<h1>Liste des fiches</h1>
<table>
 <thead>
  <tr>
   <th>Nom Fiche</th><th>Thème</th><th>Photo</th><th>Action</th>
  </tr>
 </thead>
 <tbody>
  $tr
 </tbody>
</table>

$nouveau
$nouveauTheme

HERE;
    }

    public function formFiches($_data)
    {

        $mthemes = new MThemes();
        $themes = $mthemes->SelectAll();

        if ($_data)
        {
            $nom_fiche = $_data['NOM_FICHE'];
            $contenu = $_data['CONTENU_FICHE'];
            $photo_fiche = $_data['IMAGE'];
            $selected = 'selected="selected"';
            $action = '../Php/admin.php?EX=update&ID_FICHE=' . $_data['ID_FICHE'];
            $button_delete = '<a href="../Php/admin.php?EX=delete&amp;ID_FICHE=' . $_data['ID_FICHE'] . '">Supprimer</a>';
        }
        else
            {
            $nom_fiche = '';
            $contenu = '';
            $photo_fiche = '';
            $selected = '';
            $action = '../Php/admin.php?EX=insert';
            $button_delete = '';
            }

        $options = '<option value="" ' . $selected . '" disabled="disabled">Choisissez ...</option>';

        foreach ($themes as $val)
        {
            $selected = (isset($_data['ID_THEME']) && $val['ID_THEME'] == $_data['ID_THEME']) ? ' selected="selected"' : '';
            $options .= '<option value=" ' . $val['ID_THEME'] . '"' . $selected . '>' . $val['NOM_THEME'] . '</option>';
        }

        echo <<<HERE
<form action="$action" method="post" enctype="multipart/form-data">
 <fieldset>
  <legend>Formulaire</legend>
  <p>
   <label for="nom">Nom de la fiche</label>
   <input id="NOM_FICHE" name="NOM_FICHE" value="$nom_fiche"/>
  </p>
  <p>
   <label for="contenu">Contenu</label>
   <textarea id="CONTENU_FICHE" name="CONTENU_FICHE"> $contenu </textarea>
  </p>
  <p>
   <label for="photo">Photo</label>
   <input type="file" id="PHOTO_FICHE" name="IMAGE" value=""/><br>$photo_fiche
  </p>
  <p>
   <label for="themes">Thèmes</label>
   <select name="ID_THEME">
    $options
   </select>
  </p>
  <p class="submit">
   <input type="submit" value="Ok" />
   $button_delete
  </p>
 </fieldset>
</form>
HERE;
    }

}
