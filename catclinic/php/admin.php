<?php
/**
 * Contrôleur Administration
 */

// Inclusion des constantes et des fonctions de l'application
// en particulier l'Autoload
require('../Inc/require.inc.php');

// variable de contrôle
$EX = isset($_REQUEST['EX']) ? $_REQUEST['EX'] : 'liste';

// Contrôleur
switch($EX)
{
//    case 'connexion'      : connexion();      break;
    case 'liste'          : liste();          break;
    case 'theme'          : theme();          break;
    case 'veto'           : veto();           break;
    case 'form'           : form();           break;
    case 'formTheme'      : formTheme();      break;
    case 'formVeto'       : formVeto();       break;
    case 'insert'         : insert();         break;
    case 'insertTheme'    : insertTheme();    break;
    case 'insertVeto'     : insertVeto();     break;
    case 'update'         : update();         break;
    case 'updateTheme'    : updateTheme();    break;
    case 'updateVeto'     : updateVeto();     break;
    case 'delete'         : delete();         break;
    case 'deleteTheme'    : deleteTheme();    break;
    case 'deleteVeto'     : deleteVeto();     break;
}

// Mise en page
require('../View/layout.view.php');

/*function connexion()
{
    global $content;

    $content['title'] = 'Connexion';
    $content['class'] = 'VHtml';
    $content['method'] = 'showHtml';
    $content['arg'] = '../Html/connexion.html';

    return;
} // connexion()*/

function liste()
{
    $fiches = new MFiches();
    $data = $fiches->SelectAll();

    global $content;

    $content['title'] = 'Liste des fiches';
    $content['class'] = 'VFiches';
    $content['method'] = 'showFiches';
    $content['arg'] = $data;

    return;
}
function theme()
{
    global $content;

    $theme = new MThemes();
    $data = $theme->SelectAll();


    $content['title'] = 'Liste des thèmes';
    $content['class'] = 'VThemes';
    $content['method'] = 'showThemes';
    $content['arg'] = $data;

    return;
}
function veto()
{
    global $content;

    $veto = new MVetos();
    $data = $veto->SelectAll();


    $content['title'] = 'Liste des Vétérinaires';
    $content['class'] = 'VVetos';
    $content['method'] = 'showVetos';
    $content['arg'] = $data;

    return;
}

function form()
{
    global $content;
    $data = '';

    if (isset($_GET['ID_FICHE']))
    {
        $mfiches = new MFiches($_GET['ID_FICHE']);
        $data = $mfiches->Select();
    }

    $content['title'] = 'Nouvelle fiche';
    $content['class'] = 'VFiches';
    $content['method'] = 'formFiches';
    $content['arg'] = $data;

    return;
}
function formTheme()
{
    global $content;
    $data = '';

    if (isset($_GET['ID_THEME']))
    {
        $mtheme = new MThemes($_GET['ID_THEME']);
        $data = $mtheme->SelectTheme();
    }

    $content['title'] = 'Formulaire Thème';
    $content['class'] = 'VThemes';
    $content['method'] = 'formThemes';
    $content['arg'] = $data;

    return;
}
function formVeto()
{
    global $content;
    $data = '';

    if (isset($_GET['ID_VETO']))
    {
        $mveto = new MVetos($_GET['ID_VETO']);
        $data = $mveto->SelectVeto();
    }

    $content['title'] = 'Formulaire Vétérinaire';
    $content['class'] = 'VVetos';
    $content['method'] = 'formVeto';
    $content['arg'] = $data;

    return;
}

function insert()
{
    $value = $_POST;

    if(isset($_FILES['IMAGE']['size']) && $_FILES['IMAGE']['size'] > 0)
    {
        $filename = move_file();
        $value['IMAGE'] = $filename;
    }
    else
    {
        $value['IMAGE'] = null;
    }

    $mform = new MFiches();
    $mform->SetValue($value);
    $mform->Insert();

    liste();
    return;
}
function insertTheme()
{
    $mtheme = new MThemes();
    $mtheme->SetValue($_POST);
    $mtheme->InsertTheme();

    theme();
    return;
}
function insertVeto()
{
    $value = $_POST;
    if(isset($_FILES['IMAGE']['size']) && $_FILES['IMAGE']['size'] > 0){
        $filename = move_file();

        $value['IMAGE'] = $filename;
    }
    else $value['IMAGE'] = null;

    $mveto = new MVetos();
    $mveto->SetValue($value);
    $mveto->InsertVeto();

    veto();
    return;
}

function update()
{
    $value = $_POST;
    $newFile = false;
    if(isset($_FILES['IMAGE']['size']) && $_FILES['IMAGE']['size'] > 0){
        $filename = move_file();

        $value['IMAGE'] = $filename;

        $newFile = true;
    }
    else $value['IMAGE'] = null;
    $mfiches = new MFiches($_GET['ID_FICHE']);
    $fiche = $mfiches->Select();
    if(isset($fiche['IMAGE']) && $fiche['IMAGE'] != null){
        $previousFile = $fiche['IMAGE'];
        $completePath = "../upload/".$previousFile;
        if($newFile){
            unlink($completePath);
        }
    }

    $mfiches->SetValue($value);
    $mfiches->Update();

    liste();
    return;

} // update
function updateTheme()
{
    $mtheme = new MThemes($_GET['ID_THEME']);
    $mtheme->SetValue($_POST);
    $mtheme->UpdateTheme();

    theme();
    return;

} // updateTheme
function updateVeto()
{
    $value = $_POST;
    $newFile = false;
    if(isset($_FILES['IMAGE']['size']) && $_FILES['IMAGE']['size'] > 0){
        $filename = move_file();

        $value['IMAGE'] = $filename;

        $newFile = true;
    }
    else $value['IMAGE'] = null;
    $mveto = new MVetos($_GET['ID_VETO']);
    $veto = $mveto->SelectVeto();
    if(isset($veto['IMAGE']) && $veto['IMAGE'] != null){
        $previousFile = $veto['IMAGE'];
        $completePath = "../upload/".$previousFile;
        if($newFile){
            unlink($completePath);
        }
    }

    $mveto->SetValue($value);
    $mveto->UpdateVeto();

    veto();
    return;

} // updateVeto

function move_file()
{
    $file_tmp = $_FILES['IMAGE']['tmp_name'];
    $file_new = upload($_FILES['IMAGE']);
    $file_dest = UPLOAD . $file_new;

    move_uploaded_file($file_tmp, $file_dest);
    return $file_new;
}

function delete()
{
    $fiche = new MFiches($_GET['ID_FICHE']);
    $fiche->Delete();
    liste();

    return;
}

function deleteTheme()
{
    $theme = new MThemes($_GET['ID_THEME']);
    $theme->DeleteTheme();

    theme();
    return;
}

function deleteVeto()
{
    $veto = new MVetos($_GET['ID_VETO']);
    $veto->DeleteVeto();

    veto();
    return;
}
