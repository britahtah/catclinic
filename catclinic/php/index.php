<?php
// Contrôleur

// Inclusion des constantes et des fonctions de l'application
// en particulier l'Autoload
require('../inc/require.inc.php');

// Crée une session nommée
// session_name('');
// session_start();

// $_SESSION['USER'] = 1;

// variable de contrôle
$EX = isset($_REQUEST['EX']) ? $_REQUEST['EX'] : 'home';

// Contrôleur
switch($EX)
{
    case 'home'       : home();         break;
    case 'clinique'   : clinique();     break;
    case 'infos'      : infos();        break;
    case 'contact'    : contact();      break;
    case 'themes'     : themes();       break;

}

// Mise en page
require('../view/layout.view.php');

// home(): Affichage de la page d'accueil
function home()
{
  global $content;

  $content['title'] = 'Cat Clinic';
  $content['class'] = 'VHtml';
  $content['method'] = 'showHtml';
  $content['arg'] = '../html/home.html';

  return;
} // home()

// home(): Affichage de la page de présentation de la clinique
function clinique()
{
  global $content;

  $content['title'] = 'Cat Clinic';
  $content['class'] = 'VHtml';
  $content['method'] = 'showHtml';
  $content['arg'] = '../html/clinique.html';

  return;
} // home()

// home(): Affichage de la page d'informations pratiques
function infos()
{
  global $content;

  $content['title'] = 'Cat Clinic - Informations pratiques';
  $content['class'] = 'VHtml';
  $content['method'] = 'showHtml';
  $content['arg'] = '../html/infos.html';

  return;
} // home()

function contact()
{
    global $content;

    $content['title'] = 'Cat Clinic - Formulaire de contact';
    $content['class'] = 'VHtml';
    $content['method'] = 'showHtml';
    $content['arg'] = '../html/form.php';

    return;
}

function themes($id_theme = null)
{
    $value['ID_THEME'] = isset($_GET['ID_THEME']) ? $_GET['ID_THEME'] : $id_theme;
    $mthemes = new MThemes();
    $mthemes->SetValue($value);
    $data = $mthemes->SelectAll();

    global $content;

    $content['title'] = 'Cat Clinic - Themes';
    $content['class'] = 'VThemes';
    $content['method'] = 'showThemes';
    $content['arg'] = $data;

    return;
}



?>