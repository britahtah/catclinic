<div class="cell">
    <h1>Formulaire de contact</h1>
    <p>Si vous n'avez pas trouvé de réponse à votre question sur notre site, utilisez le formulaire ci-dessous pour nous contacter.
        Nous vous répondrons sous 24h.</p>
</div>

<div class="cell">
    <form action="../Php/index.php?EX=sentmsg" method="POST">
        <div class="grid-x grid-padding-x">
            <fieldset class="cell">
                <label>Titre</label>
                <input type="radio" name="titre" value="Mme" id="titreMme" required><label for="titreMme">Mme</label>
                <input type="radio" name="titre" value="M" id="titreM"><label for="titreM">M</label>
            </fieldset>
            <div class="cell">
                <label>Nom</label>
                    <input type="text" name="nom" placeholder="Nom">
            </div>
            <div class="cell">
                <label>E-mail</label>
                    <input type="email" name="email" placeholder="Adresse e-mail">
            </div>
            <div class="cell">
                <label>Message</label>
                    <textarea name="message" placeholder="Votre message" rows="6"></textarea>

            </div>
            <div class="cell">
                <input class="button" type="submit" value="Envoyer">
                <input class="button" type="reset" value="Réinitialiser">
            </div>
        </div>
    </form>
</div>
